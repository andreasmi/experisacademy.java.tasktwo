import userHandling.Loop;
import fileManipulation.*;
import java.util.ArrayList;

public class Main {
    //Field for file service
    private static FileService fileService = new FileService();
    public static void main(String[] args) {
        //Starting the main loop of the program
        Loop loop = new Loop();

        //As long as the running flag is true, the program is running
        while(loop.getRunning()){
            try {
                System.out.println("\n");
                //Prompting the main menu
                int input = loop.promptMainMenu();
                //If the user quits, then this iterasion of the loop should not be done
                if(!loop.getRunning()){
                    continue;
                }
                //Invalid input check
                if(input == -1){
                    System.out.println("\n\u001B[31m " + "That is not a legal number" + " \u001B[0m\n");
                    continue;
                }
                
                //Based on user input
                switch(input){
                    case 1:
                        //Gets all files from resources and prints the names
                        System.out.println("\u001B[34mShowing files in /resources directory\u001B[0m");
                        ArrayList<String> files = fileService.getFilesInDirectory();
                        System.out.println("\n");
                        for (String file : files){
                            System.out.println(file);
                        }
                        break;
                    case 2:
                        //Gets the extension from the user
                        String extension = loop.promptFileExtension();
                        if(extension.trim().equals("")){
                            System.out.println("\u001B[31mNothing to show\u001B[0m");
                            break;
                        }
                        //Gets a list of file names and prints them to the console
                        ArrayList<String> fileExentions = fileService.getFilesByExtension(extension);
                        System.out.println("\n");
                        for (String file : fileExentions){
                            System.out.println(file);
                        }
                        break;
                    case 3:
                        //Looping a new menu
                        loop.promptManipulateMenu();
                        break;
                    default:
                        System.out.println("\u001B[31mSomething unexpected happened, please try again\u001B[0m");
                        break;
                }

            } catch (NumberFormatException e) {
                System.out.println("\n\u001B[31m You have to type a number \u001B[0m\n");
            }
        }

    }
}