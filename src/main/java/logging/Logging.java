package logging;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import fileManipulation.FileService;

public class Logging {
    
    private LocalDateTime date;
    private LocalTime startTime;
    private LocalTime endTime;

    //Start a new logging when it initilizes
    public Logging(){
        this.date = LocalDateTime.now();
        this.startTime = LocalTime.now();
    }

    //Setting the end time and formatting the output
    public void stop(String message){
        this.endTime = LocalTime.now();
        String currentDate = this.date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy'T'HH:mm"));
        long duration = Duration.between(startTime, endTime).toMillis();

        new FileService().writeToLog(currentDate + ": " + message + " The function took " + duration + "ms to execute.");
    }

}