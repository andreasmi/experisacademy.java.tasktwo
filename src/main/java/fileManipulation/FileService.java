package fileManipulation;

import java.io.*;
import java.util.ArrayList;

import logging.Logging;

/*
Every function starts a new logging at the start and ends it when it has completed
*/

public class FileService {

    public ArrayList<String> getFilesInDirectory() {
        Logging logging = new Logging();
        try {
            ArrayList<String> result = new ArrayList<>();
            //Getting files from resources and making an array
            
            
            File[] files = new File("resources/").listFiles();

            //Looping array and populating result array
            for (File file : files) {
                result.add(file.getName());
            }

            logging.stop("Getting all files in directory.");
            return result;
        } catch (Exception e) {
            System.out.println("\u001B[31m " + "Could not find the directory" + " \u001B[0m");
        }

        //Returns empty array if the directory is empty
        return new ArrayList<String>();
    }

    public ArrayList<String> getFilesByExtension(String extension){
        Logging logging = new Logging();
        try{
            ArrayList<String> result = new ArrayList<>();
            File directory = new File("resources/");
            //Looping the directory and adding only the matching files
            File[] files = directory.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(extension); //If name ends with the correct extension
                }
            });
        
            //Looping files and adding to result array
            for (File file : files) {
                result.add(file.getName());
            }
            
            logging.stop("Getting all files with the extension of '" + extension + "'");
            return result;

        } catch ( Exception ex ){
            System.out.println("\u001B[31m " + "Could not find the directory" + " \u001B[0m");
        }
        //Returns empty array if directory is empty
        return new ArrayList<String>();
    }

    public void renameFile(String currentName, String newName){
        Logging logging = new Logging();

        //The user has the freedom of not entering the extension
        //Making sure to add the extension if the user did not
        String cName = currentName;
        String nName = newName;
        if(!currentName.endsWith("txt")){
            cName += ".txt";
        }
        if(!newName.endsWith("txt")){
            nName += ".txt";
        }
        try{
            //Getting old and new file
            File sourceFile = new File("resources/"+cName);
            File destFile = new File("resources/"+nName);
    
            //Renaming
            if (sourceFile.renameTo(destFile)) {
                System.out.println("\u001B[32m\nFile renamed successfully\n\u001B[0m");
            } else {
                System.out.println("\u001B[31m\nFailed to rename file\n\u001B[0m");
            }
        }catch(Exception ex){
            System.out.println("\u001B[31m\nSomething went wrong\n\u001B[0m");
        }
        logging.stop("The file was renamed from " + currentName + " to " + newName + ".");
    }

    //Helper method to not copy paste code
    public void printCurrentTxtFiles(){
        //Getting the files in the directory that has an extension of txt
        ArrayList<String> currentFiles = getFilesByExtension("txt");
        System.out.println("\u001B[34m\nCurrent txt files in resources\n\u001B[0m");
        //Printing every file
        for(String file : currentFiles){
            System.out.println(file);
        }
    }

    public void printFileSize(String fileName){
        Logging logging = new Logging();
        String nName = fileName;
        long fileSize = 0;

        //User has freedom of not typing in the extension
        if(!fileName.endsWith("txt")){
            nName += ".txt";
        }
        try{
            //Opening file
            File file = new File("resources/"+nName);
            if(file.exists()){
                //Dividing file length by 1024 to get it to KB
                fileSize = file.length()/1024;
                System.out.println("\u001B[32m\n" + fileSize + "KB\n\u001B[0m");
            }else{
                System.out.println("\u001B[31m\nSorry, something went wrong.\n\u001B[0m");    
            }
        }catch(Exception ex){
            System.out.println("\u001B[31m\nSorry, something went wrong.\n\u001B[0m");
        }
        logging.stop("The file " + fileName + " is " + fileSize + "KB.");
    }

    public void countLines(String fileName){
        Logging logging = new Logging();
        String nName = fileName;
        int lineAmount = 0;
        //Users freedom of not adding extension
        if(!fileName.endsWith("txt")){
            nName += ".txt";
        }
        try{
            //Reading one line at the time from the correct file
            BufferedReader reader = new BufferedReader(new FileReader("resources/"+nName));
            try{
                //As long as the file still has any lines then add it to the line amount
                while (reader.readLine() != null){
                    lineAmount++;
                }
                System.out.println("\n\u001B[32mThere are " + lineAmount + " lines in this document!\n\u001B[0m");
            }catch(FileNotFoundException ex){
                System.out.println("\u001B[31m\nSorry, this file does not exist\n\u001B[0m");
            }catch(Exception ex){
                System.out.println("\u001B[31m\nSorry, something went wrong\n\u001B[0m");
            } finally {
                //Trying to close the reader
                if(reader != null){
                    reader.close();
                }
            }
        }catch(IOException ex){
            System.out.println("\u001B[31m\nSorry, could not read the file\n\u001B[0m");
        }
        logging.stop("The file " + fileName + " have " + lineAmount + " lines.");
    }

    public void countWord(String file, String word){
        Logging logging = new Logging();
        String nName = file;
        int count = 0;
        //Users freedom of not adding extensoin
        if(!file.endsWith("txt")){
            nName += ".txt";
        }
        try{
            //Reading the file line by line
            BufferedReader br = new BufferedReader(new FileReader("resources/"+nName));
            String line;
            //Making sure the word is in lower case
            String lowerCaseWord = word.toLowerCase();
            //While the document still has a line, loop it and assing the new line to line variable
            while((line = br.readLine()) != null){
                //If the line contains the word then loop it, if not do nothing
                if(line.toLowerCase().contains(lowerCaseWord)){
                    //Replacing everything except letters and numbers, (making sure that !,.#@ etc isnt doing anything)
                    String[] words = line.replaceAll("[^a-zA-Z0-9]", " ").toLowerCase().split(" ");
                    //Looping to make sure that there could be multiple occurences in one line
                    for(String w: words){
                        if(w.equals(lowerCaseWord)) count++;
                    }
                }
            }
            //Closing bufferedreader
            br.close();
            System.out.println("\n\u001B[32mThe word '" + lowerCaseWord + "' occured " + count + " times!\n\u001B[0m");
        }catch(FileNotFoundException ex){
            System.out.println("\u001B[31m\nSorry, could not find the file\n\u001B[0m");
        }catch(IOException ex){
            System.out.println("\u001B[31m\nSorry, this file does not exist\n\u001B[0m");
        }
        logging.stop("The term \"" + word + "\" was found " + count + " times.");
    }

    //Helper function so that logging class coukd be clean
    public void writeToLog(String message){
        String LOG_PATH = ".\\resources\\log.txt";
        try{
            File file = new File(LOG_PATH);
            //Creating file if it doesnt exist
            file.createNewFile();

            //Writing to file
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(message + "\n");
            //closing the writer
            bw.close();
            fw.close();
        } catch (IOException e) {
        }
    }


}
