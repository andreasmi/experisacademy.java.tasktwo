package userHandling;

import java.util.Scanner;
import fileManipulation.FileService;

public class Loop {
    
    //Fields
    private boolean running;
    private Scanner scanner = new Scanner(System.in);
    private boolean fileManipulation; //If new menu should loop or not
    private FileService fileService;

    
    public Loop() {
        this.running = true;
        this.fileManipulation = false;
        this.fileService = new FileService();
    }

    //Prompting main menu
    public int promptMainMenu() throws NumberFormatException{
        System.out.println("Choose an action");
        System.out.println("(1) List files in directory");
        System.out.println("(2) Get files by extension");
        System.out.println("(3) Manipulate .txt file");
        System.out.println("(q) Quit the program");

        try{
            //Getting and alidating input
            String userInput = scanner.nextLine();
            if(userInput.equals("q")){
                setRunning(false);
                return -1;
            }

            int intUserInput = Integer.parseInt(userInput);
            //Ternary operator to check if the user input is within the amount of choices 
            return intUserInput>=1&&intUserInput<=4?intUserInput:-1;
        } catch (NumberFormatException ex){
            throw ex;
        }

    }


    public String promptFileExtension(){
        //Prompting and getting extension
        System.out.println("\n");
        System.out.println("\u001B[34mInsert the file extension you want to get\u001B[0m");
        System.out.println("\u001B[34mEg. 'jpg or png'\u001B[0m");

        String extension = scanner.nextLine();

        return extension;
    }

    public void promptManipulateMenu(){
        //New menu to loop
        fileManipulation = true;
        while(fileManipulation){
            System.out.println("\nChoose an action");
            System.out.println("(1) Edit name");
            System.out.println("(2) Get file size");
            System.out.println("(3) Amount of lines");
            System.out.println("(4) Search word");
            System.out.println("(b) Go back");

            try{
                //Getting and validating user input
                String userInput = scanner.nextLine();
                if(userInput.equals("b")){
                    fileManipulation = false;
                    continue;
                }
                int intUserInput = Integer.parseInt(userInput);
                if(intUserInput > 4 || intUserInput < 1){
                    System.out.println("That is an illegal number!");
                    continue;
                }
                switch (intUserInput){
                    case 1:
                        //Getting userinput and renaming file
                        fileService.printCurrentTxtFiles();
                        System.out.println("\n\u001B[34mWrite in the current name\u001B[0m");
                        String currentName = scanner.nextLine();
                        System.out.println("\n\u001B[34mWrite in the new name\u001B[0m");
                        String newName = scanner.nextLine();
                        fileService.renameFile(currentName, newName);
                        break;
                    case 2:
                        //Getting input and printing the size
                        fileService.printCurrentTxtFiles();
                        System.out.println("\n\u001B[34mWrite the name of the file you want to get the size of\u001B[0m");
                        String name = scanner.nextLine();
                        fileService.printFileSize(name);
                        break;
                    case 3:
                        //Getting input and counting lines
                        fileService.printCurrentTxtFiles();
                        System.out.println("\n\u001B[34mWrite the name of the file you want to count (remember the extension)\u001B[0m");
                        String fileName = scanner.nextLine();
                        fileService.countLines(fileName);
                        break;
                    case 4:
                        //Getting input nad counting a sepcific word
                        fileService.printCurrentTxtFiles();
                        System.out.println("\n\u001B[34mWrite the name of the file you want to count a word in (remember the extension)\u001B[0m");
                        fileName = scanner.nextLine();
                        System.out.println("\n\u001B[34mWhich word do you want to search for?\u001B[0m");
                        String word = scanner.nextLine();
                        fileService.countWord(fileName, word);
                        break;
                    default:
                        System.out.println("\u001B[31mSomething unexpected happened, please try again\u001B[0m");
                        break;
                }
            } catch (NumberFormatException ex){
                System.out.println("Sorry, you have to enter a number!");
                continue;
            }
        }
        return;
    }


    //Getter and setter for thre main lopp flag
    public boolean getRunning(){return this.running;}
    public void setRunning(boolean running){this.running = running;}



}
