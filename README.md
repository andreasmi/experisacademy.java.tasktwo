# ExperisAcademy Java Task Two
Task 2 of the java part of the fullstack java accelerate course on Noroff.

## Usage
Clone/Download the repository, and run `java -jar Main.jar`.

## Compile images
![Javac command](out/ASSIGNMENT SCREENSHOTS/javac command.png)
![Jar and Java -jar command](/out/ASSIGNMENT SCREENSHOTS/jar and java -jar command.png)

## Known bug
The Main.jar file needs to be in the same folder as a directory named 'resources'. If not, the program can't find a directory to search through or save a log file to.